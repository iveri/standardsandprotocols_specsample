/**
 * Use example:
                      
                                 Project name     
                                      |       API name          
                                      |           |
 * node cli/createServiceDoc.js SomeProject UserManagement 2
 */
const fs = require("fs")

const projectName = process.argv[2]
const serviceName = process.argv[3]
const documentationVersion = (process.argv[4] || 0) | 0

const serviceFolder = serviceName + "API"

const apiTemplateFunc = (() => {

    switch (documentationVersion) {
        case 0:
        case 1:
            return serviceAPITemplate

        case 2:
            return serviceAPITemplate_2
    }
})()

fs.mkdirSync(serviceFolder)
fs.writeFileSync(`${serviceFolder}/${serviceName}API.ts`, apiTemplateFunc(projectName, serviceName))
fs.writeFileSync(`${serviceFolder}/${serviceName}DB.ts`, serviceDBTemplate(projectName, serviceName))
fs.writeFileSync(`${serviceFolder}/Types.ts`, serviceTypesTemplate(projectName, serviceName))


function serviceDBTemplate(projectName, serviceName) {
    const str = `/// <reference path="../standardsandprotocols.idl_types_for_db_documentation/DBIDL.ts" />

namespace ${projectName}.${serviceName}DB {
    type DB_GUID = DB.Column<DB.GUID, DB.NotNullable, DB.AutoIncrement>
    type DB_VarChar = DB.Column<DB.VarChar, DB.NotNullable, DB.AutoIncrement>

    export class Users implements DB.Table {
        associatedTypes?: any

        id: DB_GUID
        firstTimeLoggedIn:DB.Column<DB.Integer, DB.Nullable, DB.NoAutoIncrement>
        passwordHash: DB.Column<DB.VarChar, DB.NotNullable, DB.NoAutoIncrement>
    }

    export class UserAttributes implements DB.Table{
        associatedTypes?: any

        id: DB_GUID
        key:DB.Column<DB.Integer, DB.Nullable, DB.NoAutoIncrement>
        value: DB.Column<DB.VarChar, DB.NotNullable, DB.NoAutoIncrement>        
        userId: DB.ForeignKey<Users, DB.NotNullable> = (table)=>table.id
    }

}`

    return str
}

function serviceTypesTemplate(projectName, serviceName) {
    const str = `/// <reference path="${serviceName}API.ts" />

declare namespace ${serviceName}.Types {

    export type TimeStamp = number

    export interface TypeAndDataSrouce<Type, DataSourceType> {
        _913a4036ae4d46b99901decdb9034800: any
    }
}`

    return str
}

function serviceAPITemplate(projectName, serviceName) {
    const str = `/// <reference path="../standardsandprotocols.jsonmessaging/JSONMessaging.ts" />
/// <reference path="Types.ts" />

namespace ${projectName}.${serviceName}API {

    enum MethodScope {
        AdminPanel,
        Client
    }

    interface JSONMessageScoped extends JSONMessage {
        scope: Array<MethodScope>
    }

    export enum Methods {
        /**
         * @type {Messages.DummyMethod}
         */
        DummyMethod = 0,
        /**
         * @type {Messages.DummyMethodResult}
         */
        DummyMethodResult = 1,
    }

    export namespace Messages {

        export class DummyMethod implements JSONMessage {
            method = Methods.DummyMethod
            type = JSONMessageType.Request
            params: {
                paramA: string,
                paramB: number
            }
        }

        export class DummyMethodResult implements JSONMessage {
            method = Methods.DummyMethodResult
            type = JSONMessageType.Response
            params: {
                someResult:{
                    fieldA:string,
                    fieldB:number
                }
            }
            errorCode:${serviceName}ErrorCode
        }   
    }

    export enum ${serviceName}ErrorCode {
        None = 0,
        SomeError = 1,
        SomeOtherError = 2
    }
}`

    return str
}

function serviceAPITemplate_2(projectName, serviceName) {
    const str = `/// <reference path="../standardsandprotocols.jsonmessaging/JSONMessaging.ts" />
/// <reference path="Types.ts" />

namespace ${projectName}.${serviceName}API {

    enum MethodScope {
        AdminPanel,
        Client
    }

    interface JSONMessageScoped extends JSONMessage {
        scope: Array<MethodScope>
    }

    export namespace Messages {

        export class DummyMethod implements JSONMessage {
            method = 10
            type = JSONMessageType.Request
            params: {
                paramA: string,
                paramB: number
            }
        }

        export class DummyMethodResult implements JSONMessage {
            method = 11
            type = JSONMessageType.Response
            params: {
                someResult:{
                    fieldA:string,
                    fieldB:number
                }
            }
            errorCode:${serviceName}ErrorCode
        }   
    }

    export enum ${serviceName}ErrorCode {
        None = 0,
        SomeError = 1,
        SomeOtherError = 2
    }
}`

    return str
}

